//
//  EkycViewModel.swift
//  EkycFramework
//
//  Created by Nguyen Thanh Duc on 2/12/20.
//  Copyright © 2020 Wee. All rights reserved.
//

import Foundation
import Ekyc

protocol WEEEkycDelegate: class {
    func NextPointDelegate(_: CurrentStatusPoint) -> Void
    func currentStatusDelegate(_: CurrentStatusPoint) -> Void
    func timeoutStatusDelegate(_: CurrentStatusPoint) -> Void
    func finishStatusDelegate() -> Void
}

class EkycViewModel: NSObject {
        
    weak var delegate: WEEEkycDelegate?
    let messageQueue = DispatchQueue.init(label: "message_queue", qos: DispatchQoS.userInitiated, attributes: DispatchQueue.Attributes.concurrent, autoreleaseFrequency: DispatchQueue.AutoreleaseFrequency.inherit, target: nil)
    
    func reqInitLibrary()  {
        var errPointer: NSError?
//        let jsonEncoder = JSONEncoder()
//        let jsonDataRect = try? jsonEncoder.encode(rect)
//        let jsonDataTopLeft = try? jsonEncoder.encode(_topLeft)
//        let jsonDataLeft = try? jsonEncoder.encode(left)
//        let jsonDataBottomLeft = try? jsonEncoder.encode(bottomLeft)
//        let jsonDataTopRight = try? jsonEncoder.encode(topRight)
//        let jsonDataRight = try? jsonEncoder.encode(right)
//        let jsonDataBottomRight = try? jsonEncoder.encode(bottomRight)
        
        EkycInitLibrary(self, &errPointer)
    }
    
    func reqExtractFrontInformation(front: Data, cardType: String, completeHandler: @escaping ((ExtractFrontIDCardStatus) -> Void)) {
        messageQueue.async {
            var errPointer: NSError?
            let data: Data? = EkycExtractFrontInformation(front, cardType, &errPointer)
            if let unwrapData: Data = data {
                do {
                    let decoder = JSONDecoder()
                    let resp = try decoder.decode(ReqEkyc.Response.RespExtractFrontData.self, from: unwrapData)
                    DispatchQueue.main.async {
                        if resp.Code == 0 {
                            completeHandler(.SUCCESS(resp))
                        }else {
                            completeHandler(.FAILED(resp))
                        }
                    }
                }catch {
                    DispatchQueue.main.async {
                        let resp = ReqEkyc.Response.RespExtractFrontData(SessionID: "", Number: "", BirthDay: "", Code: -1, Message: error.localizedDescription, FullName: "", HomeTown: "", Address: "")
                        completeHandler(.UNKNOWERROR(resp))
                    }
                }
            }
        }
    }
    
    func reqExtractBackInformation(back: Data, cardType: String, completeHandler: @escaping ((ExtractBackIDCardStatus) -> Void)) {
        messageQueue.async {
            var errPointer: NSError?
            let data: Data? = EkycExtractBackInformation(back, cardType, &errPointer)
            if let unwrapData: Data = data {
                do {
                    let decoder = JSONDecoder()
                    let resp = try decoder.decode(ReqEkyc.Response.RespExtractBackData.self, from: unwrapData)
                    DispatchQueue.main.async {
                        if resp.Code == 0 {
                            completeHandler(.SUCCESS(resp))
                        }else {
                            completeHandler(.FAILED(resp))
                        }
                    }
                }catch {
                    DispatchQueue.main.async {
                        let resp = ReqEkyc.Response.RespExtractBackData(Code: -1, Message: error.localizedDescription, Valid: "")
                        completeHandler(.UNKNOWERROR(resp))
                    }
                }
            }
        }
    }
    
    func reqStartLivenessCheck() -> Bool {
        var errPointer: NSError?
        EkycStartLivenessCheck(&errPointer)
        if errPointer != nil {
            return false
        }
        
        return true
    }
    
    func reqUpdateStatusPoint(isSuccess: Bool) {
        messageQueue.async {
            var errPointer: NSError?
            EkycUpdateDirectionStatus(isSuccess, &errPointer)
        }
    }
    
    func reqNextDirection() {
        messageQueue.async {
            var errPointer: NSError?
            EkycGetNextDirection(&errPointer)
        }
    }
}

extension EkycViewModel: EkycDelegateProtocol {
    func nextDirection(_ data: Data?) {
        if let unwrapData: Data = data {
            do {
                let encoder = JSONDecoder()
                let resp = try encoder.decode(ReqEkyc.Response.RespCurrentStatusPoint.self, from: unwrapData)
                if let d = self.delegate {
                    DispatchQueue.main.async {
                        d.NextPointDelegate(.SUCCESS(resp))
                    }
                }
            }catch {
                if let d = self.delegate {
                    DispatchQueue.main.async {
                        let resp = ReqEkyc.Response.RespCurrentStatusPoint(position: -1, status: false)
                        d.NextPointDelegate(.UNKNOWERROR(resp))
                    }
                }
            }
        }
    }
    
    func currentDirection(_ data: Data?) {
        if let unwrapData: Data = data {
            do {
                let encoder = JSONDecoder()
                let resp = try encoder.decode(ReqEkyc.Response.RespCurrentStatusPoint.self, from: unwrapData)
                if let d = self.delegate {
                    DispatchQueue.main.async {
                        d.currentStatusDelegate(.SUCCESS(resp))
                    }
                }
            }catch {
                
            }
        }
    }
    
    func timeoutDirection(_ data: Data?) {
        if let unwrapData: Data = data {
            do {
                let encoder = JSONDecoder()
                let resp = try encoder.decode(ReqEkyc.Response.RespCurrentStatusPoint.self, from: unwrapData)
                if let d = self.delegate {
                    DispatchQueue.main.async {
                        d.timeoutStatusDelegate(.SUCCESS(resp))
                    }
                }
            }catch {
                
            }
        }
    }
    
    func finishLivenessCheck() {
        if let d = self.delegate {
            DispatchQueue.main.async {
                d.finishStatusDelegate()
            }
        }
    }
}
