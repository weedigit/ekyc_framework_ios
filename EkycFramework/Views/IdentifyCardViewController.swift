//
//  IdentifyCardViewController.swift
//  weeid
//
//  Created by LV on 2/8/20.
//  Copyright © 2020 LV. All rights reserved.
//

import UIKit
import Ekyc

public class IdentifyCardViewController: UIViewController {
    
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var transparentHolesView: UIView!
    @IBOutlet weak var statusBarHeight: NSLayoutConstraint!
    @IBOutlet weak var rectHoles: UIView!
    @IBOutlet weak var imgIDCardView: UIImageView!
    @IBOutlet weak var hintCameraLbl: UILabel!
    
    @IBOutlet weak var cameraActionContentView: UIView!
    @IBOutlet weak var desLbl: UILabel!
    @IBOutlet weak var captureBtn: UIButton!
    @IBOutlet weak var usedPhotoBtn: UIButton!
    @IBOutlet weak var retakeBtn: UIButton!
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var desLoading: UILabel!
    
    @IBOutlet weak var croppingFrame: UIView!
    private let cameraController = CameraController()
    private var idCardImg = CIImage()
    private var useFrontSide = false
    
    let desPhotoCapture = "Make sure your ID Card is without any glare and fully inside"
    let desUsedPhoto = "Is your document fully visible, glare free and not blurred ?"
    
    var extractFrontData: ReqEkyc.Response.RespExtractFrontData?
    var extractBackData: ReqEkyc.Response.RespExtractBackData?
    
    public override var prefersStatusBarHidden: Bool {
        return false
    }
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        usedPhotoBtn.layer.cornerRadius = 25
        activityIndicator.startAnimating()
        // setting rabbit ear status bar
        statusBarHeight.constant = UIApplication.shared.statusBarFrame.height
        // setup camera
        self.initCamera()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.resetData()
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIScreen.main.brightness = CGFloat(1.0)
        setupHolesRect()
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        cameraController.stopCamera()
        self.idCardImg = CIImage()
    }
    
    private func initCamera() {
        cameraController.prepare {(error) in
            if let error = error {
                print("configureCameraController", error.localizedDescription)
            }
            try? self.cameraController.displayPreview(on: self.cameraView)
            self.cameraController.delegate = self
            self.cameraController.startCamera()
            self.activityIndicator.stopAnimating()
            self.loadingView.isHidden = true
        }
    }
    
    private func setupHolesRect() {
        let roundedRect = rectHoles.frame
        let path = UIBezierPath(rect: transparentHolesView.bounds)
        let croppedPath = UIBezierPath(rect: roundedRect)
        path.append(croppedPath)
        path.usesEvenOddFillRule = true

        let mask = CAShapeLayer()
        mask.path = path.cgPath
        mask.fillRule = .evenOdd
        transparentHolesView.layer.mask = mask
    }
    
    @IBAction func btnCaptureAction(_ sender: Any) {
        let image: UIImage = convertCIImageToUIImage(cmage: idCardImg)
        let rootImg: UIImage = image.imageRotatedByDegrees(degrees: 0, flip: true)!
        
        let factorX = rootImg.size.width/view.frame.width
        let factorY = rootImg.size.height/view.frame.height
        let rect = CGRect(x: (croppingFrame.frame.minX) * factorX, y: (croppingFrame.frame.minY) * factorY, width: (croppingFrame.frame.width) * factorX, height: (croppingFrame.frame.height) * factorY)
        let croppedImage = rootImg.croppedInRect(rect: rect)
        imgIDCardView.image = croppedImage
        desLbl.text = desUsedPhoto
        captureBtn.isHidden = true
        usedPhotoBtn.isHidden = false
        retakeBtn.isHidden = false
    }
    
    // Convert CIImage to UIImage
    func convertCIImageToUIImage(cmage:CIImage) -> UIImage {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
    @IBAction func usedPhotoAction(_ sender: Any) {
        activityIndicator.startAnimating()
        loadingView.isHidden = false
        
        // Call Model => nhan ket qua
        if (useFrontSide) { // mat sau
            let image = imgIDCardView.image
            if let img = image {
                let backData = img.pngData()
                if let data = backData {
                    let vm = EkycViewModel()
                    vm.reqExtractBackInformation(back: data, cardType: "cccd") { [unowned self] (result) in
                        switch result {
                        case .SUCCESS(let resp):
                            self.extractBackData = resp
                            DispatchQueue.main.async {
                                let podBundle = Bundle(for: WEECameraView.self)
                                let bundleURL = podBundle.url(forResource: "EkycFramework", withExtension: "bundle")
                                let bundle = Bundle(url: bundleURL!)!
                                
                                let storyboard = UIStoryboard(name: "Result", bundle: bundle)
                                let viewController = storyboard.instantiateViewController(withIdentifier: "extract_id_card_vc") as? WEEExtractIDCardResultViewController
                                viewController?.extractFrontData = self.extractFrontData
                                viewController?.extractBackData = self.extractBackData
                                if let vc = viewController {
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                            break
                        case .FAILED(_):
                            self.retakePhoto()
                            break
                        case .UNKNOWERROR(_):
                            self.retakePhoto()
                            break
                        }
                    }
                }
            }
        } else {// Mat truoc
            hintCameraLbl.text = "Behind side"
            desLbl.text = desPhotoCapture
            captureBtn.isHidden = false
            usedPhotoBtn.isHidden = true
            retakeBtn.isHidden = true
            let image = imgIDCardView.image
            if let img = image {
                let frontData = img.pngData()
                if let data = frontData {
                    let vm = EkycViewModel()
                    vm.reqExtractFrontInformation(front: data, cardType: "cccd") { [unowned self] (result) in
                        switch result {
                        case .SUCCESS(let resp):
                            self.extractFrontData = resp
                            DispatchQueue.main.async {
                                self.loadingView.isHidden = true
                                self.imgIDCardView.image = nil
                                self.useFrontSide = true
                            }
                            break
                        case .FAILED(_):
                            self.retakePhoto()
                            break
                        case .UNKNOWERROR(_):
                            self.retakePhoto()
                            break
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func retakePhotoAction(_ sender: Any) {
        self.retakePhoto()
    }
    
    fileprivate func retakePhoto() {
        imgIDCardView.image = UIImage()
        captureBtn.isHidden = false
        usedPhotoBtn.isHidden = true
        retakeBtn.isHidden = true
        desLbl.text = desPhotoCapture
    }
    
    fileprivate func resetData() {
        imgIDCardView.image = nil
        captureBtn.isHidden = false
        usedPhotoBtn.isHidden = true
        retakeBtn.isHidden = true
        desLbl.text = desPhotoCapture
        useFrontSide = false
        self.hintCameraLbl.text = "Front side"
        
        self.cameraController.startCamera()
        self.activityIndicator.stopAnimating()
        self.loadingView.isHidden = true
    }
}

extension IdentifyCardViewController: CameraControllerDelegate {
    func getImage(img: CIImage) {
        self.idCardImg = img
    }
}
