//
//  CameraController.swift
//  AV Foundation
//
//  Created by Pranjal Satija on 29/5/2017.
//  Copyright © 2017 AppCoda. All rights reserved.
//

import AVFoundation
import UIKit
import ImageIO
import Foundation
import AssetsLibrary
import AVKit
import Firebase

protocol CameraControllerDelegate: class {
//    func getFaceBounds(bounds: CGRect?)
    func getImage(img: CIImage)
}

class CameraController: NSObject {
    var captureSession: AVCaptureSession?
    
    var currentCameraPosition: CameraPosition?
    
    var frontCamera: AVCaptureDevice?
    var frontCameraInput: AVCaptureDeviceInput?
    
    var photoOutput: AVCapturePhotoOutput?
    var bufferOutput: AVCaptureVideoDataOutput?
    var movieOutput = AVCaptureMovieFileOutput()
    fileprivate lazy var audioDataOutput = AVCaptureAudioDataOutput()
    var metadataOutput = AVCaptureMetadataOutput()
    
    private lazy var vision = Vision.vision()
    var autoMLOnDeviceLabeler: VisionImageLabeler!
    
    private lazy var annotationOverlayView: UIView = {
        //        precondition(isViewLoaded)
        let annotationOverlayView = UIView(frame: .zero)
        annotationOverlayView.translatesAutoresizingMaskIntoConstraints = false
        return annotationOverlayView
    }()
    
    var rearCamera: AVCaptureDevice?
    var rearCameraInput: AVCaptureDeviceInput?
    
    var previewLayer: AVCaptureVideoPreviewLayer?
    
    var flashMode = AVCaptureDevice.FlashMode.off
    var photoCaptureCompletionBlock: ((UIImage?, Error?) -> Void)?
    
    var isFaceRecognition: Bool = false
    
    weak var delegate: CameraControllerDelegate? = nil
    
    
    var ciFaceDetector: CIDetector? = nil
    
    var frameCount = 0
    var videoDataOutputQueue: DispatchQueue = DispatchQueue(label: "VideoDataOutputQueue", qos: .background)
    var inProgressLabel: Bool = false
    
    fileprivate(set) lazy var isRecording = false
    fileprivate var videoWriter: AVAssetWriter!
    fileprivate var videoWriterInput: AVAssetWriterInput!
    fileprivate var audioWriterInput: AVAssetWriterInput!
    fileprivate var sessionAtSourceTime: CMTime?
    private var hasFace = false
    private var timer: Timer? = nil
    
    lazy var lastSampleTime: CMTime = {
        let lastSampleTime = CMTime.zero
        return lastSampleTime
    }()
    
    lazy var isRecordingVideo: Bool = {
        let isRecordingVideo = false
        return isRecordingVideo
    }()
    
    var videoOutputFullFileName: String?
    
    func startCamera() {
        if let s = self.captureSession {
            if !s.isRunning {
                s.startRunning()
            }
        }
    }
    
    func stopCamera() {
        if let s = self.captureSession {
            if s.isRunning {
                s.stopRunning()
            }
        }
    }
}

extension CameraController {
    func prepare(completionHandler: @escaping (Error?) -> Void) {
        
        loadMLModel()
        func createCaptureSession() {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (videoGranted: Bool) -> Void in
                if (videoGranted) {
                    //Do Your stuff here
                    
                } else {
                    // Rejected Camera
                }
            })
            self.captureSession = AVCaptureSession()
            let sessionPreset = AVCaptureSession.Preset.high
            self.captureSession?.sessionPreset = sessionPreset
        }
        
        func configureCaptureDevices() throws {
            
            let session = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back)
            
            let cameras = session.devices.compactMap { $0 }
            guard !cameras.isEmpty else { throw CameraControllerError.noCamerasAvailable }
            
            for camera in cameras {
                if camera.position == .front {
                    self.frontCamera = camera
                }
                
                if camera.position == .back {
                    self.rearCamera = camera
                    
                    try camera.lockForConfiguration()
                    camera.focusMode = .continuousAutoFocus
                    camera.unlockForConfiguration()
                }
            }
        }
        
        func configureDeviceInputs() throws {
            guard let captureSession = self.captureSession else { throw CameraControllerError.captureSessionIsMissing }
            
            //            if let audioInput = AVCaptureDevice.default(for: AVMediaType.audio) {
            //                captureSession.addInput(try AVCaptureDeviceInput(device: audioInput))
            //            }
            
            if let rearCamera = self.rearCamera {
                self.rearCameraInput = try AVCaptureDeviceInput(device: rearCamera)
                if captureSession.canAddInput(self.rearCameraInput!) { captureSession.addInput(self.rearCameraInput!) }
                
                self.currentCameraPosition = .rear
            }
                
            else if let frontCamera = self.frontCamera {
                self.frontCameraInput = try AVCaptureDeviceInput(device: frontCamera)
                
                if captureSession.canAddInput(self.frontCameraInput!) { captureSession.addInput(self.frontCameraInput!) }
                else { throw CameraControllerError.inputsAreInvalid }
                
                self.currentCameraPosition = .front
            }
                
            else { throw CameraControllerError.noCamerasAvailable }
        }
        
        func configurePhotoOutput() throws {
            guard let captureSession = self.captureSession else { throw CameraControllerError.captureSessionIsMissing }
            
            if captureSession.canAddOutput(self.metadataOutput) {
                captureSession.addOutput(self.metadataOutput)
                let queue = DispatchQueue(label: "captureQueue", qos: .background)
                metadataOutput.setMetadataObjectsDelegate(self, queue: queue)
                if Set([.face]).isSubset(of: metadataOutput.availableMetadataObjectTypes) {
                    metadataOutput.metadataObjectTypes = [.face]
                }
            }
            
            self.photoOutput = AVCapturePhotoOutput()
            self.photoOutput!.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecJPEG])], completionHandler: nil)
            
            if captureSession.canAddOutput(self.photoOutput!) { captureSession.addOutput(self.photoOutput!) }
        }
        
        func configureBufferOutput() throws {
            guard let captureSession = self.captureSession else { throw CameraControllerError.captureSessionIsMissing }
            self.bufferOutput = AVCaptureVideoDataOutput()
            bufferOutput?.videoSettings = [
                ((kCVPixelBufferPixelFormatTypeKey as NSString) as String): NSNumber(value: kCVPixelFormatType_32BGRA)
            ]
            bufferOutput?.alwaysDiscardsLateVideoFrames = true
            
            guard let bufferOutput = self.bufferOutput else { return }
            if captureSession.canAddOutput(bufferOutput) {
                self.captureSession?.addOutput(bufferOutput)
            }
            
            self.captureSession?.commitConfiguration()
            let queue = DispatchQueue(label: "captureQueue", qos: .background)
            bufferOutput.setSampleBufferDelegate(self, queue: queue)
        }
        
        func activateProximitySensor() {
            let device = UIDevice.current
            device.isProximityMonitoringEnabled = true
            if device.isProximityMonitoringEnabled {
                NotificationCenter.default.addObserver(self, selector: #selector(proximityChanged(notification:)), name: NSNotification.Name(rawValue: "UIDeviceProximityStateDidChangeNotification"), object: device)
            }
        }
        
        func setupCIFaceDetector() {
            let detectorOptions = [CIDetectorAccuracyLow : CIDetectorAccuracy]
            self.ciFaceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: detectorOptions)
        }
        
        DispatchQueue(label: "prepare").async {
            do {
                createCaptureSession()
                try configureCaptureDevices()
                try configureDeviceInputs()
                try configurePhotoOutput()
                try configureBufferOutput()
                setupCIFaceDetector()
            }
                
            catch {
                DispatchQueue.main.async {
                    completionHandler(error)
                }
                
                return
            }
            
            DispatchQueue.main.async {
                completionHandler(nil)
            }
        }
    }
    
    @objc func proximityChanged(notification: NSNotification) {
        if let device = notification.object as? UIDevice {
            print("\(device) detected!")
        }
    }
    
    func displayPreview(on view: UIView) throws {
        guard let captureSession = self.captureSession else { throw CameraControllerError.captureSessionIsMissing }
        
        self.previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        self.previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.previewLayer?.connection?.videoOrientation = .portrait
        
        view.layer.insertSublayer(self.previewLayer!, at: 0)
        self.previewLayer?.frame = view.bounds
    }
    
    func switchCameras() throws {
        guard let currentCameraPosition = currentCameraPosition, let captureSession = self.captureSession, captureSession.isRunning else { throw CameraControllerError.captureSessionIsMissing }
        
        captureSession.beginConfiguration()
        
        func switchToFrontCamera() throws {
            
            guard let rearCameraInput = self.rearCameraInput, captureSession.inputs.contains(rearCameraInput),
                let frontCamera = self.frontCamera else { throw CameraControllerError.invalidOperation }
            
            self.frontCameraInput = try AVCaptureDeviceInput(device: frontCamera)
            
            captureSession.removeInput(rearCameraInput)
            
            if captureSession.canAddInput(self.frontCameraInput!) {
                captureSession.addInput(self.frontCameraInput!)
                
                self.currentCameraPosition = .front
            }
                
            else {
                throw CameraControllerError.invalidOperation
            }
        }
        
        func switchToRearCamera() throws {
            
            guard let frontCameraInput = self.frontCameraInput, captureSession.inputs.contains(frontCameraInput),
                let rearCamera = self.rearCamera else { throw CameraControllerError.invalidOperation }
            
            self.rearCameraInput = try AVCaptureDeviceInput(device: rearCamera)
            
            captureSession.removeInput(frontCameraInput)
            
            if captureSession.canAddInput(self.rearCameraInput!) {
                captureSession.addInput(self.rearCameraInput!)
                
                self.currentCameraPosition = .rear
            }
                
            else { throw CameraControllerError.invalidOperation }
        }
        
        switch currentCameraPosition {
        case .front:
            try switchToRearCamera()
            
        case .rear:
            try switchToFrontCamera()
        }
        
        captureSession.commitConfiguration()
    }
    
    func captureImage(completion: @escaping (UIImage?, Error?) -> Void) {
        guard let captureSession = captureSession, captureSession.isRunning else { completion(nil, CameraControllerError.captureSessionIsMissing); return }
        
        let settings = AVCapturePhotoSettings()
        settings.flashMode = self.flashMode
        
        self.photoOutput?.capturePhoto(with: settings, delegate: self)
        self.photoCaptureCompletionBlock = completion
    }
    
    func proccess(every: Int, callback: () -> Void) {
        frameCount = frameCount + 1
        // Process every nth frame
        if(frameCount % every == 0) {
            callback()
        }
    }
    
    func exifOrientation(_ orientation: UIDeviceOrientation) -> NSNumber? {
        var exifOrientation: Int
        /* kCGImagePropertyOrientation values
         The intended display orientation of the image. If present, this key is a CFNumber value with the same value as defined
         by the TIFF and EXIF specifications -- see enumeration of integer constants.
         The value specified where the origin (0,0) of the image is located. If not present, a value of 1 is assumed.
         
         used when calling featuresInImage: options: The value for this key is an integer NSNumber from 1..8 as found in kCGImagePropertyOrientation.
         If present, the detection will be done based on that orientation but the coordinates in the returned features will still be based on those of the image. */
        enum Enum1 : Int {
            case photos_EXIF_0ROW_TOP_0COL_LEFT = 1
            //   1  =  0th row is at the top, and 0th column is on the left (THE DEFAULT).
            case photos_EXIF_0ROW_TOP_0COL_RIGHT = 2
            //   2  =  0th row is at the top, and 0th column is on the right.
            case photos_EXIF_0ROW_BOTTOM_0COL_RIGHT = 3
            //   3  =  0th row is at the bottom, and 0th column is on the right.
            case photos_EXIF_0ROW_BOTTOM_0COL_LEFT = 4
            //   4  =  0th row is at the bottom, and 0th column is on the left.
            case photos_EXIF_0ROW_LEFT_0COL_TOP = 5
            //   5  =  0th row is on the left, and 0th column is the top.
            case photos_EXIF_0ROW_RIGHT_0COL_TOP = 6
            //   6  =  0th row is on the right, and 0th column is the top.
            case photos_EXIF_0ROW_RIGHT_0COL_BOTTOM = 7
            //   7  =  0th row is on the right, and 0th column is the bottom.
            case photos_EXIF_0ROW_LEFT_0COL_BOTTOM = 8
        }
        
        switch orientation {
        case .portraitUpsideDown:
            // Device oriented vertically, home button on the top
            exifOrientation = Enum1.photos_EXIF_0ROW_LEFT_0COL_BOTTOM.rawValue
        case .landscapeLeft:
            // Device oriented horizontally, home button on the right
            exifOrientation = Enum1.photos_EXIF_0ROW_BOTTOM_0COL_RIGHT.rawValue
        case .landscapeRight:
            // Device oriented horizontally, home button on the left
            exifOrientation = Enum1.photos_EXIF_0ROW_TOP_0COL_LEFT.rawValue
        case .portrait:
            fallthrough
        default:
            exifOrientation = Enum1.photos_EXIF_0ROW_RIGHT_0COL_TOP.rawValue
        }
        return exifOrientation as NSNumber
    }
}

extension CameraController: AVCapturePhotoCaptureDelegate {
    public func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?,
                            resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Swift.Error?) {
        if let error = error { self.photoCaptureCompletionBlock?(nil, error) }
            
        else if let buffer = photoSampleBuffer, let data = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: buffer, previewPhotoSampleBuffer: nil),
            let image = UIImage(data: data) {
            
            self.photoCaptureCompletionBlock?(image, nil)
        }
            
        else {
            self.photoCaptureCompletionBlock?(nil, CameraControllerError.unknown)
        }
    }
}

extension CameraController: AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate {
    func convertPoint(point: CGPoint, size: CGSize) -> CGPoint {
        let p = CGPoint(x: point.x/size.width, y: point.y/size.height)
        if let prLayer = self.previewLayer {
            return prLayer.layerPointConverted(fromCaptureDevicePoint: p)
        } else {
            return CGPoint.zero
        }
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }
        
        let ciImage: CIImage = CIImage(cvImageBuffer: pixelBuffer).oriented(forExifOrientation: Int32(UIImage.Orientation.downMirrored.rawValue))

        self.delegate?.getImage(img: ciImage)
        
//        proccess(every: 10) {
//            if inProgressLabel {
//                return
//            }
//
//            inProgressLabel = true;
//
//            let cameraPosition = AVCaptureDevice.Position.back  // Set to the capture device you used.
//            let metadata = VisionImageMetadata()
//            metadata.orientation = imageOrientation(
//                deviceOrientation: UIDevice.current.orientation,
//                cameraPosition: cameraPosition
//            )
//
//            let imgVision = VisionImage(buffer: sampleBuffer)
//            imgVision.metadata = metadata
//
//            let imageWidth = CGFloat(CVPixelBufferGetWidth(pixelBuffer))
//            let imageHeight = CGFloat(CVPixelBufferGetHeight(pixelBuffer))
//
//            self.autoMLOnDeviceLabeler.process(imgVision) { [unowned self] labels, error in
//                guard error == nil, let labels = labels else {
//                    print("null")
//                    return
//                }
//                for l in labels {
//                    print("Result: \(l.text) - \(l.confidence!)%")
//                }
//                self.inProgressLabel = false
//            }
//        }
        
    }
    
    func imageOrientation(
        deviceOrientation: UIDeviceOrientation,
        cameraPosition: AVCaptureDevice.Position
        ) -> VisionDetectorImageOrientation {
        switch deviceOrientation {
        case .portrait:
            return cameraPosition == .front ? .leftTop : .rightTop
        case .landscapeLeft:
            return cameraPosition == .front ? .bottomLeft : .topLeft
        case .portraitUpsideDown:
            return cameraPosition == .front ? .rightBottom : .leftBottom
        case .landscapeRight:
            return cameraPosition == .front ? .topRight : .bottomRight
        case .faceDown, .faceUp, .unknown:
            return .leftTop
        }
    }
}

extension CameraController: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
//        self.proccess(every: 10) {
//            if timer != nil {
//                timer?.invalidate()
//                timer = nil
//            }
//            for metadata in metadataObjects {
//                if let face = metadata as? AVMetadataFaceObject {
//                    self.hasFace = true
//                    guard let transformedObject = self.previewLayer?.transformedMetadataObject(for: face) else {
//                        continue
//                    }
//                    self.delegate?.getFaceBounds(bounds: self.increaseRect(rect: transformedObject.bounds, byPercentage: 0.5))
//                } else {
//                    self.delegate?.getFaceBounds(bounds: nil)
//                }
//            }
//            DispatchQueue.main.async {
//                self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.actTimer), userInfo: nil, repeats: false)
//            }
//        }
    }
    @objc fileprivate func actTimer() {
        hasFace = false
    }
}

extension CameraController {
    
    func loadMLModel() {
        let podBundle = Bundle(for: CameraController.self)
        let bundleURL = podBundle.url(forResource: "EkycFramework", withExtension: "bundle")
        let bundle = Bundle(url: bundleURL!)!
        
        let path = bundle.path(forResource: Constants.localModelManifestFileName, ofType: Constants.autoMLManifestFileType)
        if let p = path {
            let localModel = AutoMLLocalModel(manifestPath: p)
            let options = VisionOnDeviceAutoMLImageLabelerOptions(localModel: localModel)
            options.confidenceThreshold = Constants.labelConfidenceThreshold
            
            self.autoMLOnDeviceLabeler = vision.onDeviceAutoMLImageLabeler(options: options)
        }
       }
    
    func increaseRect(rect: CGRect, byPercentage percentage: CGFloat) -> CGRect {
        let startWidth = rect.width
        let startHeight = rect.height
        let adjustmentWidth = (startWidth * percentage) / 2.0
        let adjustmentHeight = (startHeight * percentage) / 2.0
        return rect.insetBy(dx: -adjustmentWidth, dy: -adjustmentHeight)
    }
    
    func convertCIImageToUIImage(cmage:CIImage) -> UIImage {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
    enum CameraControllerError: Swift.Error {
        case captureSessionAlreadyRunning
        case captureSessionIsMissing
        case inputsAreInvalid
        case invalidOperation
        case noCamerasAvailable
        case unknown
    }
    
    public enum CameraPosition {
        case front
        case rear
    }
    
    private enum Constants {
      static let modelExtension = "tflite"
      static let detectionNoResultsMessage = "No results returned."
      static let failedToDetectObjectsMessage = "Failed to detect objects in image."
      static let localModelManifestFileName = "manifest"
      static let autoMLManifestFileType = "json"
      static let labelConfidenceThreshold: Float = 0.5
    }
}
