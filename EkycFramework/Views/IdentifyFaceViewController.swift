//
//  IdentifyFaceViewController.swift
//  weeid
//
//  Created by LV on 2/9/20.
//  Copyright © 2020 LV. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase

public class IdentifyFaceViewController: UIViewController {
    //---
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var transparentHolesView: UIView!
    @IBOutlet weak var rectHoles: UIView!
    @IBOutlet weak var statusBarHeight: NSLayoutConstraint!
    @IBOutlet weak var arc_RightBottom: UIView!
    @IBOutlet weak var arc_LeftBottom: UIView!
    @IBOutlet weak var arc_LeftCenter: UIView!
    @IBOutlet weak var arc_LeftTop: UIView!
    @IBOutlet weak var arc_RightTop: UIView!
    @IBOutlet weak var arc_RightCenter: UIView!
    
    @IBOutlet weak var hintView: UIView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgVector: UIImageView!
    
    // test GUI
    @IBOutlet weak var livenessCheckGUI: UIView!
    @IBOutlet weak var lineCheckGUI: UIView!
    
    
    let shapeLayer_RightBottom = CAShapeLayer()
    let shapeLayer_LeftBottom = CAShapeLayer()
    let shapeLayer_LeftCenter = CAShapeLayer()
    let shapeLayer_LeftTop = CAShapeLayer()
    let shapeLayer_RightTop = CAShapeLayer()
    let shapeLayer_RightCenter = CAShapeLayer()
    
    // test GUI
    let shapeLayerZone = CAShapeLayer()
    let shapeLayerLine = CAShapeLayer()
    //--
    var nextRequestArcProccess: Bool = false
    var listLivenessCheckStatus: [Bool] = []
    var isArcRequestCheck: ARC_CIRCLE = ARC_CIRCLE.NONE
    var curArcCheck: ARC_CIRCLE = ARC_CIRCLE.NONE
    var centerFrontFaceMockDevice: CGPoint = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
    
    
    private lazy var vision = Vision.vision()
    var faceDetector: VisionFaceDetector!
    
    // MARK: Camera declare start
    //    private let cameraController = CameraController()
    var input: AVCaptureDeviceInput!
    var captureSession: AVCaptureSession!
    var camera: AVCaptureDevice!
    private var previewLayer: AVCaptureVideoPreviewLayer!
    // MARK: camera declare end
    
    var imgArray = [UIImage]()
    var uiimg: UIImage = UIImage()
    var exportAV : Bool = true
    let screenRect = UIScreen.main.bounds
    var frameCount = 0
    private var timer: Timer? = nil
    var fakeCount: Int = 0
    //---
    private var isLivenessCheck: Bool = false
    var isFaceProcessing: Bool = false
    var isFirstFace: Bool = true
    // MARK: view frame
    var faceBox: UIView!
    var centerFaceBox: UIView!
    var detectZoneBox: UIView!
    
    let viewModel: EkycViewModel = EkycViewModel()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.setupValue()
        // setup camera
        self.initCamera()
        self.viewModel.delegate = self
    }
    
    private func setupDebugGUI() {
        // test GUI
        self.drawTestCircle()
        
        faceBox = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        faceBox.layer.borderWidth = 3
        faceBox.layer.borderColor = UIColor.red.cgColor
        faceBox.backgroundColor = UIColor.clear
        self.view.addSubview(faceBox)
        
        centerFaceBox = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        centerFaceBox.layer.borderWidth = 3
        centerFaceBox.layer.borderColor = UIColor.red.cgColor
        centerFaceBox.backgroundColor = UIColor.clear
        self.view.addSubview(centerFaceBox)
        
        detectZoneBox = UIView(frame: CGRect(x: self.view.frame.width/2-Constant.LIVENESS_CHECK_RADIUS_DETECT_FRONT_FACE, y: self.view.frame.height/2-Constant.LIVENESS_CHECK_RADIUS_DETECT_FRONT_FACE, width: Constant.LIVENESS_CHECK_RADIUS_DETECT_FRONT_FACE*2, height: Constant.LIVENESS_CHECK_RADIUS_DETECT_FRONT_FACE*2))
        detectZoneBox.layer.cornerRadius = Constant.LIVENESS_CHECK_RADIUS_DETECT_FRONT_FACE
        detectZoneBox.layer.borderWidth = 2
        detectZoneBox.layer.borderColor = UIColor.blue.cgColor
        detectZoneBox.backgroundColor = UIColor.clear
        self.view.addSubview(detectZoneBox)
    }
    
    private func convertNumToArc(num: Int) -> ARC_CIRCLE {
        switch num {
        case 1:
            return ARC_CIRCLE.LEFT_TOP
        case 2:
            return ARC_CIRCLE.LEFT_CENTER
        case 3:
            return ARC_CIRCLE.LEFT_BOTTOM
        case 4:
            return ARC_CIRCLE.RIGHT_TOP
        case 5:
            return ARC_CIRCLE.RIGHT_CENTER
        case 6:
            return ARC_CIRCLE.RIGHT_BOTTOM
        default:
            return ARC_CIRCLE.LEFT_TOP
        }
    }
    
    private func setArcRequestCheck(arcNumber: ARC_CIRCLE) {
        switch arcNumber {
        case ARC_CIRCLE.LEFT_TOP:
            shapeLayer_LeftTop.strokeColor = Colour.arcSelectedColor
            break
        case ARC_CIRCLE.LEFT_CENTER:
            shapeLayer_LeftCenter.strokeColor = Colour.arcSelectedColor
            break
        case ARC_CIRCLE.LEFT_BOTTOM:
            shapeLayer_LeftBottom.strokeColor = Colour.arcSelectedColor
            break
        case ARC_CIRCLE.RIGHT_TOP:
            shapeLayer_RightTop.strokeColor = Colour.arcSelectedColor
            break
        case ARC_CIRCLE.RIGHT_CENTER:
            shapeLayer_RightCenter.strokeColor = Colour.arcSelectedColor
            break
        case ARC_CIRCLE.RIGHT_BOTTOM:
            shapeLayer_RightBottom.strokeColor = Colour.arcSelectedColor
            break
        default:
            break
        }
    }
    
    private func animArcSelected(arcNumber: ARC_CIRCLE, successful: Bool) {
        let positionConvert = rectHoles.convert(rectHoles.center, from: transparentHolesView)
        
        let pathAnimation = CABasicAnimation(keyPath: "path")
//        pathAnimation.toValue = circlePath.cgPath
        pathAnimation.duration = Constant.ANIMATION_TIME
        pathAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        pathAnimation.autoreverses = true
        
        let colorAnimation = CABasicAnimation(keyPath: "strokeColor")
        colorAnimation.toValue = UIColor.green.cgColor
        colorAnimation.duration = Constant.ANIMATION_TIME
        colorAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        colorAnimation.autoreverses = true
        
        switch arcNumber {
        case ARC_CIRCLE.LEFT_TOP:
            let circlePath: UIBezierPath = UIBezierPath(arcCenter: positionConvert, radius: (rectHoles.frame.height + Constant.LIVENESS_MIN_SPACE_ANIM_CIRCLE)/2, startAngle: CGFloat(205 * Double.pi / 180), endAngle: CGFloat(245 * Double.pi / 180), clockwise: true)
            pathAnimation.toValue = circlePath.cgPath
            if (successful) {
                shapeLayer_LeftTop.add(colorAnimation, forKey: "strokeColor")
            }
            shapeLayer_LeftTop.add(pathAnimation, forKey: "path")
            break
        case ARC_CIRCLE.LEFT_CENTER:
            let circlePath: UIBezierPath = UIBezierPath(arcCenter: positionConvert, radius: (rectHoles.frame.height + Constant.LIVENESS_MIN_SPACE_ANIM_CIRCLE)/2, startAngle: CGFloat(160 * Double.pi / 180), endAngle: CGFloat(200 * Double.pi / 180), clockwise: true)
            pathAnimation.toValue = circlePath.cgPath
            if (successful) {
                shapeLayer_LeftCenter.add(colorAnimation, forKey: "strokeColor")
            }
            shapeLayer_LeftCenter.add(pathAnimation, forKey: "path")
            break
        case ARC_CIRCLE.LEFT_BOTTOM:
            let circlePath: UIBezierPath = UIBezierPath(arcCenter: positionConvert, radius: (rectHoles.frame.height + Constant.LIVENESS_MIN_SPACE_ANIM_CIRCLE)/2, startAngle: CGFloat(115 * Double.pi / 180), endAngle: CGFloat(155 * Double.pi / 180), clockwise: true)
            pathAnimation.toValue = circlePath.cgPath
            if (successful) {
                shapeLayer_LeftBottom.add(colorAnimation, forKey: "strokeColor")
            }
            shapeLayer_LeftBottom.add(pathAnimation, forKey: "path")
            break
        case ARC_CIRCLE.RIGHT_TOP:
            let circlePath: UIBezierPath = UIBezierPath(arcCenter: positionConvert, radius: (rectHoles.frame.height + Constant.LIVENESS_MIN_SPACE_ANIM_CIRCLE)/2, startAngle: CGFloat(295 * Double.pi / 180), endAngle: CGFloat(335 * Double.pi / 180), clockwise: true)
            pathAnimation.toValue = circlePath.cgPath
            if (successful) {
                shapeLayer_RightTop.add(colorAnimation, forKey: "strokeColor")
            }
            shapeLayer_RightTop.add(pathAnimation, forKey: "path")
            break
        case ARC_CIRCLE.RIGHT_CENTER:
            let circlePath: UIBezierPath = UIBezierPath(arcCenter: positionConvert, radius: (rectHoles.frame.height + Constant.LIVENESS_MIN_SPACE_ANIM_CIRCLE)/2, startAngle: CGFloat(340 * Double.pi / 180), endAngle: CGFloat(380 * Double.pi / 180), clockwise: true)
            pathAnimation.toValue = circlePath.cgPath
            if (successful) {
                shapeLayer_RightCenter.add(colorAnimation, forKey: "strokeColor")
            }
            shapeLayer_RightCenter.add(pathAnimation, forKey: "path")
            break
        case ARC_CIRCLE.RIGHT_BOTTOM:
            let circlePath: UIBezierPath = UIBezierPath(arcCenter: positionConvert, radius: (rectHoles.frame.height + Constant.LIVENESS_MIN_SPACE_ANIM_CIRCLE)/2, startAngle: CGFloat(25 * Double.pi / 180), endAngle: CGFloat(65 * Double.pi / 180), clockwise: true)
            pathAnimation.toValue = circlePath.cgPath
            if (successful) {
                shapeLayer_RightBottom.add(colorAnimation, forKey: "strokeColor")
            }
            shapeLayer_RightBottom.add(pathAnimation, forKey: "path")
            break
        default:
            break
        }
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateFinishAnimColor), userInfo: nil, repeats: false)
    }
    
    @objc func updateFinishAnimColor() {
        // do what should happen when timer triggers an event
        switch self.curArcCheck {
        case ARC_CIRCLE.LEFT_TOP:
            shapeLayer_LeftTop.strokeColor = Colour.arcDefaultColor
            break
        case ARC_CIRCLE.LEFT_CENTER:
            shapeLayer_LeftCenter.strokeColor = Colour.arcDefaultColor
            break
        case ARC_CIRCLE.LEFT_BOTTOM:
            shapeLayer_LeftBottom.strokeColor = Colour.arcDefaultColor
            break
        case ARC_CIRCLE.RIGHT_TOP:
            shapeLayer_RightTop.strokeColor = Colour.arcDefaultColor
            break
        case ARC_CIRCLE.RIGHT_CENTER:
            shapeLayer_RightCenter.strokeColor = Colour.arcDefaultColor
            break
        case ARC_CIRCLE.RIGHT_BOTTOM:
            shapeLayer_RightBottom.strokeColor = Colour.arcDefaultColor
            break
        default:
            break
        }
    }
    
    private func setupAnimVectorRotate(arcCircle: ARC_CIRCLE) {
        var degree: CGFloat = 0
        
        switch arcCircle {
        case ARC_CIRCLE.LEFT_TOP:
            degree = 225*CGFloat.pi/180
            break
        case ARC_CIRCLE.LEFT_CENTER:
            degree = 180*CGFloat.pi/180
            break
        case ARC_CIRCLE.LEFT_BOTTOM:
            degree = 135*CGFloat.pi/180
            break
        case ARC_CIRCLE.RIGHT_TOP:
            degree = 315*CGFloat.pi/180
            break
        case ARC_CIRCLE.RIGHT_CENTER:
            degree = 0*CGFloat.pi/180
            break
        case ARC_CIRCLE.RIGHT_BOTTOM:
            degree = 45*CGFloat.pi/180
            break
        default:
            break
        }
        
        UIView.animate(withDuration: Constant.ANIMATION_TIME, delay: 0.0, options: [], animations: {
            self.imgVector.transform = CGAffineTransform(rotationAngle: degree)
        })
    }
    
    private func animationVectorFinish(arcCircle: ARC_CIRCLE) {
        var degree: CGFloat = 0
        switch arcCircle {
        case ARC_CIRCLE.LEFT_TOP:
            degree = 225/360*2*CGFloat.pi
            break
        case ARC_CIRCLE.LEFT_CENTER:
            degree = 180/360*2*CGFloat.pi
            break
        case ARC_CIRCLE.LEFT_BOTTOM:
            degree = 135/360*2*CGFloat.pi
            break
        case ARC_CIRCLE.RIGHT_TOP:
            degree = 315/360*2*CGFloat.pi
            break
        case ARC_CIRCLE.RIGHT_CENTER:
            degree = 0/360*2*CGFloat.pi
            break
        case ARC_CIRCLE.RIGHT_BOTTOM:
            degree = 45/360*2*CGFloat.pi
            break
        default:
            break
        }
        let r = (self.view.frame.width - 60)/2
        UIView.animate(withDuration: Constant.ANIMATION_TIME, delay: 0.0, options: [], animations: {
            self.imgVector.layer.position = CGPoint(x: (self.view.frame.width/2 - 10) + cos(degree)*r, y: (self.view.frame.height/2 - 10) + sin(degree)*r)
        },
        completion: { finished in
            UIView.animate(withDuration: Constant.ANIMATION_TIME, delay: 0.0, options: [], animations: {
                self.imgVector.layer.position = CGPoint(x: self.view.frame.width/2, y: self.view.frame.height/2+10)
            })
        })
    }
    
    private func setupValue() {
        // setup UI
        hintView.layer.cornerRadius = 11
        // setting rabbit ear status bar
        statusBarHeight.constant = UIApplication.shared.statusBarFrame.height
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupUI()
        self.setupDebugGUI()
        self.viewModel.reqInitLibrary()
        _ = self.viewModel.reqStartLivenessCheck()
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopCamera()
    }
    
    @IBAction func closeHint(_ sender: Any) {
        self.hintView.isHidden = true
    }
    
    private func setupUI() {
        self.setupHolesRect()
//        self.drawCircleRedHoles()
        self.drawArc_RightBottom()
        self.drawArc_LeftBottom()
        self.drawArc_LeftCenter()
        self.drawArc_LeftTop()
        self.drawArc_RightTop()
        self.drawArc_RightCenter()
    }
    
    private func drawTestCircle() {
        let positionConvert = rectHoles.convert(rectHoles.center, from: transparentHolesView)
        let circlePath: UIBezierPath = UIBezierPath(arcCenter: positionConvert, radius: Constant.LIVENESS_CHECK_RADIUS_ZONE, startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        
        self.shapeLayerZone.position = CGPoint(x: 0, y: 0)
        self.shapeLayerZone.path = circlePath.cgPath

        //color inside circle
        self.shapeLayerZone.fillColor = UIColor.clear.cgColor
        //colored border of circle
        self.shapeLayerZone.strokeColor = UIColor.red.cgColor
        //width size of border
        self.shapeLayerZone.lineWidth = 5

        livenessCheckGUI.layer.addSublayer(self.shapeLayerZone)
    }
    
    private func drawArc_RightCenter() {
        let positionConvert = arc_RightCenter.convert(arc_RightCenter.center, from: transparentHolesView)
        let circlePath: UIBezierPath = UIBezierPath(arcCenter: positionConvert, radius: (arc_RightCenter.frame.height + Constant.LIVENESS_SPACE_CIRCLE)/2, startAngle: CGFloat(340 * Double.pi / 180), endAngle: CGFloat(380 * Double.pi / 180), clockwise: true)

        shapeLayer_RightCenter.position = CGPoint(x: 0, y: 0)
        shapeLayer_RightCenter.path = circlePath.cgPath
        shapeLayer_RightCenter.lineCap = CAShapeLayerLineCap.round
        //color inside circle
        shapeLayer_RightCenter.fillColor = UIColor.clear.cgColor
        //colored border of circle
        shapeLayer_RightCenter.strokeColor = Colour.arcDefaultColor
        //width size of border
        shapeLayer_RightCenter.lineWidth = 7

        arc_RightCenter.layer.addSublayer(shapeLayer_RightCenter)
    }
    
    private func drawArc_RightTop() {
        let positionConvert = arc_RightTop.convert(arc_RightTop.center, from: transparentHolesView)
        let circlePath: UIBezierPath = UIBezierPath(arcCenter: positionConvert, radius: (arc_RightTop.frame.height + Constant.LIVENESS_SPACE_CIRCLE)/2, startAngle: CGFloat(295 * Double.pi / 180), endAngle: CGFloat(335 * Double.pi / 180), clockwise: true)

        shapeLayer_RightTop.position = CGPoint(x: 0, y: 0)
        shapeLayer_RightTop.path = circlePath.cgPath
        shapeLayer_RightTop.lineCap = CAShapeLayerLineCap.round
        //color inside circle
        shapeLayer_RightTop.fillColor = UIColor.clear.cgColor
        //colored border of circle
        shapeLayer_RightTop.strokeColor = Colour.arcDefaultColor
        //width size of border
        shapeLayer_RightTop.lineWidth = 7

        arc_RightTop.layer.addSublayer(shapeLayer_RightTop)
    }
    
    private func drawArc_LeftTop() {
        let positionConvert = arc_LeftTop.convert(arc_LeftTop.center, from: transparentHolesView)
        let circlePath: UIBezierPath = UIBezierPath(arcCenter: positionConvert, radius: (arc_LeftTop.frame.height + Constant.LIVENESS_SPACE_CIRCLE)/2, startAngle: CGFloat(205 * Double.pi / 180), endAngle: CGFloat(245 * Double.pi / 180), clockwise: true)
        
        shapeLayer_LeftTop.position = CGPoint(x: 0, y: 0)
        shapeLayer_LeftTop.path = circlePath.cgPath
        shapeLayer_LeftTop.lineCap = CAShapeLayerLineCap.round
        //color inside circle
        shapeLayer_LeftTop.fillColor = UIColor.clear.cgColor
        //colored border of circle
        shapeLayer_LeftTop.strokeColor = Colour.arcDefaultColor
        //width size of border
        shapeLayer_LeftTop.lineWidth = 7

        arc_LeftTop.layer.addSublayer(shapeLayer_LeftTop)
    }
    
    private func drawArc_LeftCenter() {
        let positionConvert = arc_LeftCenter.convert(arc_LeftCenter.center, from: transparentHolesView)
        let circlePath: UIBezierPath = UIBezierPath(arcCenter: positionConvert, radius: (arc_LeftCenter.frame.height + Constant.LIVENESS_SPACE_CIRCLE)/2, startAngle: CGFloat(160 * Double.pi / 180), endAngle: CGFloat(200 * Double.pi / 180), clockwise: true)

        shapeLayer_LeftCenter.position = CGPoint(x: 0, y: 0)
        shapeLayer_LeftCenter.path = circlePath.cgPath
        shapeLayer_LeftCenter.lineCap = CAShapeLayerLineCap.round
        //color inside circle
        shapeLayer_LeftCenter.fillColor = UIColor.clear.cgColor
        //colored border of circle
        shapeLayer_LeftCenter.strokeColor = Colour.arcDefaultColor
        //width size of border
        shapeLayer_LeftCenter.lineWidth = 7

        arc_LeftCenter.layer.addSublayer(shapeLayer_LeftCenter)
    }
    
    private func drawArc_LeftBottom() {
        let positionConvert = arc_LeftBottom.convert(arc_LeftBottom.center, from: transparentHolesView)
        let circlePath: UIBezierPath = UIBezierPath(arcCenter: positionConvert, radius: (arc_LeftBottom.frame.height + Constant.LIVENESS_SPACE_CIRCLE)/2, startAngle: CGFloat(115 * Double.pi / 180), endAngle: CGFloat(155 * Double.pi / 180), clockwise: true)

        shapeLayer_LeftBottom.position = CGPoint(x: 0, y: 0)
        shapeLayer_LeftBottom.path = circlePath.cgPath
        shapeLayer_LeftBottom.lineCap = CAShapeLayerLineCap.round
        //color inside circle
        shapeLayer_LeftBottom.fillColor = UIColor.clear.cgColor
        //colored border of circle
        shapeLayer_LeftBottom.strokeColor = Colour.arcDefaultColor
        //width size of border
        shapeLayer_LeftBottom.lineWidth = 7

        arc_LeftBottom.layer.addSublayer(shapeLayer_LeftBottom)
    }
    
    private func drawArc_RightBottom() {
        let positionConvert = arc_RightBottom.convert(arc_RightBottom.center, from: transparentHolesView)
        let circlePath: UIBezierPath = UIBezierPath(arcCenter: positionConvert, radius: (arc_RightBottom.frame.height + Constant.LIVENESS_SPACE_CIRCLE)/2, startAngle: CGFloat(25 * Double.pi / 180), endAngle: CGFloat(65 * Double.pi / 180), clockwise: true)

        shapeLayer_RightBottom.position = CGPoint(x: 0, y: 0)
        shapeLayer_RightBottom.path = circlePath.cgPath
        shapeLayer_RightBottom.lineCap = CAShapeLayerLineCap.round
        //color inside circle
        shapeLayer_RightBottom.fillColor = UIColor.clear.cgColor
        //colored border of circle
        shapeLayer_RightBottom.strokeColor = Colour.arcDefaultColor
        //width size of border
        shapeLayer_RightBottom.lineWidth = 7

        arc_RightBottom.layer.addSublayer(shapeLayer_RightBottom)
    }
    
    private func drawCircleRedHoles() {
        let positionConvert = rectHoles.convert(rectHoles.center, from: transparentHolesView)
        let circlePath: UIBezierPath = UIBezierPath(arcCenter: positionConvert, radius: CGFloat(self.rectHoles.frame.height/2), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        
        let shapeLayerRed = CAShapeLayer()
        shapeLayerRed.position = CGPoint(x: 0, y: 0)
        shapeLayerRed.path = circlePath.cgPath

        //color inside circle
        shapeLayerRed.fillColor = UIColor.clear.cgColor
        //colored border of circle
        shapeLayerRed.strokeColor = UIColor.red.cgColor
        //width size of border
        shapeLayerRed.lineWidth = 10

        rectHoles.layer.addSublayer(shapeLayerRed)
    }
    
    private func setupHolesRect() {
        let roundedRect = rectHoles.frame
        let cornerRadius = roundedRect.size.height / 2
        let path = UIBezierPath(rect: transparentHolesView.bounds)
        let croppedPath = UIBezierPath(roundedRect: roundedRect, cornerRadius: cornerRadius)
        path.append(croppedPath)
        path.usesEvenOddFillRule = true

        let mask = CAShapeLayer()
        mask.path = path.cgPath
        mask.fillRule = .evenOdd
        transparentHolesView.layer.mask = mask
    }
    
    private func initCamera() {
        setupVision()
        setupCamera()
    }
    
    private func setupCamera(){
        self.captureSession = AVCaptureSession()
        let sessionPreset = AVCaptureSession.Preset.high
        self.captureSession?.sessionPreset = sessionPreset
        
        camera = AVCaptureDevice.default(
            .builtInWideAngleCamera,
            for: .video,
            position: .front)
        
        do {
            input = try AVCaptureDeviceInput(device: camera)
            
        } catch let error as NSError {
            print(error)
        }
        
        if captureSession.canAddInput(input) {
            captureSession.addInput(input)
        }
        
        let output = AVCaptureVideoDataOutput()
        output.videoSettings =
            [(kCVPixelBufferPixelFormatTypeKey as String): kCVPixelFormatType_32BGRA]
        
        if captureSession.canAddOutput(output) {
            captureSession.addOutput(output)
        }
        captureSession.commitConfiguration()
       
        let queue = DispatchQueue(label: "output.queue")
        output.setSampleBufferDelegate(self, queue: queue)
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = screenRect
        previewLayer.videoGravity = .resizeAspectFill
        cameraView.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
        fakeCount = 0
    }
    
    public func stopCamera() {
        self.imgArray.removeAll()
        captureSession.stopRunning()
        
        for output in captureSession.outputs {
            captureSession.removeOutput(output)
        }
        
        for input in captureSession.inputs {
            captureSession.removeInput(input)
        }
        captureSession = nil
        camera = nil
        fakeCount = 0
    }
    
    func proccess(every: Int, callback: () -> Void) {
        frameCount = frameCount + 1
        // Process every nth frame
        if(frameCount % every == 0) {
            callback()
        }
    }
    
    public func exportVideo() {
//        let settings = CXEImagesToVideo.videoSettings(codec: AVVideoCodecH264, width: Int(imgArray[0].size.width), height: Int(imgArray[0].size.height))
//        let movieMaker = CXEImagesToVideo(videoSettings: settings)
//        movieMaker.createMovieFrom(images: imgArray){ (fileURL) in
//            print(fileURL)
//            self.imgArray.removeAll()
//        }
    }
}

extension IdentifyFaceViewController {
    private func setupVision() {
        let options = VisionFaceDetectorOptions()
        options.landmarkMode = .all
        options.isTrackingEnabled = true
        faceDetector = vision.faceDetector(options: options)
    }
    
    private func detectFacesOnDevice(in image: VisionImage, width: CGFloat, height: CGFloat) {
        faceDetector.process(image) { features, error in
            if let error = error {
                print(error.localizedDescription)
                self.isFaceProcessing = false
                return
            }
            
            guard error == nil, let features = features, !features.isEmpty else {
//                print("On-Device face detector returned no results.")
                self.isFaceProcessing = false
                return
            }
            
            // step 1 get largeFace, front face and check face zone
            var largeFace: VisionFace = features[0]
            for face in features {
                if (largeFace.frame.width < face.frame.width) {
                    largeFace = face
                }
            }
            
            // step 2 check front face
            let isFrontFace = self.checkFrontFace(face: largeFace)
            // step 3 check face zone
            let isZoneFace = self.checkZoneFace(face: largeFace)
            // step 4 converts the center of an identity circle
            if (isFrontFace && isZoneFace) {
                self.isLivenessCheck = true
                let faceRectDevice = self.convertRectFaceVisionToDevice(face: largeFace)
                self.centerFrontFaceMockDevice = CGPoint(x: faceRectDevice.midX, y: faceRectDevice.midY)
                
                let convertPointIdentityircle = self.lineCheckGUI.convert(self.centerFrontFaceMockDevice, from: self.view)
                let circlePath: UIBezierPath = UIBezierPath(arcCenter: convertPointIdentityircle, radius: Constant.LIVENESS_CHECK_RADIUS_ZONE, startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
                
                self.shapeLayerZone.path = circlePath.cgPath
                self.shapeLayerZone.strokeColor = UIColor.green.cgColor
                
                // first face
                if (self.isFirstFace) {
                    self.isFirstFace = false
                    _ = self.viewModel.reqNextDirection()
                }
                
                if (!self.isFirstFace && self.nextRequestArcProccess) {
                    self.nextRequestArcProccess = false
                    _ = self.viewModel.reqNextDirection()
//                    print("whhyyyyyyyyyyyyyyyyyyyyyyyyy")
                }
            }
            // step 5
            //--
            if (self.isLivenessCheck) {
                self.livenessCheckAction(face: largeFace)
            }
            
            // old check code
            let normalizedRect = CGRect(
                x: largeFace.frame.origin.x / width,
                y: largeFace.frame.origin.y / height,
                width: largeFace.frame.size.width / width,
                height: largeFace.frame.size.height / height
            )
            let standardizedRect =
                self.previewLayer.layerRectConverted(fromMetadataOutputRect: normalizedRect).standardized
            self.faceBox?.frame = standardizedRect

            self.centerFaceBox?.frame = CGRect(x: standardizedRect.midX - 5, y: standardizedRect.midY - 5, width: 10, height: 10)
            //--
            self.isFaceProcessing = false
        }
    }
    
    func checkFrontFace(face: VisionFace) -> Bool{
        return checkLandMark(face: face)
    }
    
    func checkLandMark(face: VisionFace) -> Bool {
        if (Constant.LIVENESS_CHECK_ROTATE_FRONT_FACE_MIN <= face.headEulerAngleY && face.headEulerAngleY <= Constant.LIVENESS_CHECK_ROTATE_FRONT_FACE_MAX && Constant.LIVENESS_CHECK_ROTATE_FRONT_FACE_MIN <= face.headEulerAngleZ && face.headEulerAngleZ <= Constant.LIVENESS_CHECK_ROTATE_FRONT_FACE_MAX) {
            return true
        }
        return false
    }
    
    func checkZoneFace(face: VisionFace) -> Bool {
        let rectFace = convertRectFaceVisionToDevice(face: face)
        let centerCircle = CGPoint(x: self.view.frame.width/2, y: self.view.frame.height/2)
        let centerPointFace = CGPoint(x: rectFace.midX, y: rectFace.midY)
//        if (rectFace.width < self.view.frame.width && distanceBetweenPoints(a: centerCircle, b: centerPointFace) < Constant.LIVE_NESS_CHECK_RADIUS_ZONE) {
//            return true
//        }
        if (rectFace.width < (self.view.frame.width + 500) && distanceBetweenPoints(a: centerCircle, b: centerPointFace) < Constant.LIVENESS_CHECK_RADIUS_ZONE) {
            return true
        }
        return false
    }
    
    private func endLivenessCheck() {
        isLivenessCheck = false
        self.shapeLayerZone.strokeColor = UIColor.red.cgColor
    }
    
    private func livenessCheckAction(face: VisionFace) {
        if (self.checkZoneLivenessCheck(face: face) && livenessCheck(face: face)) {
            self.endLivenessCheck()
            let centerCircle = CGPoint(x: self.view.frame.width/2, y: self.view.frame.height/2)
            let rectFace = convertRectFaceVisionToDevice(face: face)
            let centerPointFace = CGPoint(x: rectFace.midX, y: rectFace.midY)
            
            var arcSelected: ARC_CIRCLE = ARC_CIRCLE.NONE
            if (centerPointFace.x > centerCircle.x && centerPointFace.y > centerCircle.y) {
                let centerPointCheckFaceConvert = CGPoint(x: self.centerFrontFaceMockDevice.x - (self.centerFrontFaceMockDevice.x - centerCircle.x), y: self.centerFrontFaceMockDevice.y - (self.centerFrontFaceMockDevice.y - centerCircle.y))
                let centerFaceConvert = CGPoint(x: self.centerFrontFaceMockDevice.x - (self.centerFrontFaceMockDevice.x - centerPointFace.x), y: self.centerFrontFaceMockDevice.y - (self.centerFrontFaceMockDevice.y - centerPointFace.y))
                let angle = calculateQuadrantI(centre: centerPointCheckFaceConvert, point: centerFaceConvert)
                if (checkAngularCoordinatesTest(radian: angle, point: centerFaceConvert) == ARC_CIRCLE.RIGHT_BOTTOM) {
                    arcSelected = ARC_CIRCLE.RIGHT_BOTTOM
                } else if (checkAngularCoordinatesTest(radian: angle, point: centerFaceConvert) == ARC_CIRCLE.RIGHT_CENTER) {
                    arcSelected = ARC_CIRCLE.RIGHT_CENTER
                }
            } else if (centerPointFace.x < centerCircle.x && centerPointFace.y > centerCircle.y) {
                let centerPointCheckFaceConvert = CGPoint(x: self.centerFrontFaceMockDevice.x + (centerCircle.x - self.centerFrontFaceMockDevice.x), y: self.centerFrontFaceMockDevice.y - (self.centerFrontFaceMockDevice.y - centerCircle.y))
                let centerFaceConvert = CGPoint(x: self.centerFrontFaceMockDevice.x + (centerPointFace.x - self.centerFrontFaceMockDevice.x), y: self.centerFrontFaceMockDevice.y - (self.centerFrontFaceMockDevice.y - centerPointFace.y))
                let angle = calculateQuadrantII(centre: centerPointCheckFaceConvert, point: centerFaceConvert)
                if (checkAngularCoordinatesTest(radian: angle, point: centerFaceConvert) == ARC_CIRCLE.LEFT_BOTTOM) {
                    arcSelected = ARC_CIRCLE.LEFT_BOTTOM
                } else if (checkAngularCoordinatesTest(radian: angle, point: centerFaceConvert) == ARC_CIRCLE.LEFT_CENTER) {
                    arcSelected = ARC_CIRCLE.LEFT_CENTER
                }
            } else if (centerPointFace.x < centerCircle.x && centerPointFace.y < centerCircle.y) {
                let centerPointCheckFaceConvert = CGPoint(x: self.centerFrontFaceMockDevice.x + (centerCircle.x - self.centerFrontFaceMockDevice.x), y: self.centerFrontFaceMockDevice.y + (centerCircle.y - self.centerFrontFaceMockDevice.y))
                let centerFaceConvert = CGPoint(x: self.centerFrontFaceMockDevice.x + (centerPointFace.x - self.centerFrontFaceMockDevice.x), y: self.centerFrontFaceMockDevice.y + (centerPointFace.y - self.centerFrontFaceMockDevice.y))
                let angle = calculateQuadrantIII(centre: centerPointCheckFaceConvert, point: centerFaceConvert)
                if (checkAngularCoordinatesTest(radian: angle, point: centerFaceConvert) == ARC_CIRCLE.LEFT_TOP) {
                    arcSelected = ARC_CIRCLE.LEFT_TOP
                } else if (checkAngularCoordinatesTest(radian: angle, point: centerFaceConvert) == ARC_CIRCLE.LEFT_CENTER) {
                    arcSelected = ARC_CIRCLE.LEFT_CENTER
                }
            } else if (centerPointFace.x > centerCircle.x && centerPointFace.y < centerCircle.y) {
                let centerPointCheckFaceConvert = CGPoint(x: self.centerFrontFaceMockDevice.x - (self.centerFrontFaceMockDevice.x - centerCircle.x), y: self.centerFrontFaceMockDevice.y + (centerCircle.y - self.centerFrontFaceMockDevice.y))
                let centerFaceConvert = CGPoint(x: self.centerFrontFaceMockDevice.x - (self.centerFrontFaceMockDevice.x - centerPointFace.x), y: self.centerFrontFaceMockDevice.y + (centerPointFace.y - self.centerFrontFaceMockDevice.y))
                let angle = calculateQuadrantIV(centre: centerPointCheckFaceConvert, point: centerFaceConvert)
                if (checkAngularCoordinatesTest(radian: angle, point: centerFaceConvert) == ARC_CIRCLE.RIGHT_TOP) {
                    arcSelected = ARC_CIRCLE.RIGHT_TOP
                } else if (checkAngularCoordinatesTest(radian: angle, point: centerFaceConvert) == ARC_CIRCLE.RIGHT_CENTER) {
                    arcSelected = ARC_CIRCLE.RIGHT_CENTER
                }
            }
            var livenessCheckStatus: Bool = false
            if (arcSelected == isArcRequestCheck) {
                livenessCheckStatus = true
            }
            self.curArcCheck = self.isArcRequestCheck
            self.animArcSelected(arcNumber: curArcCheck, successful: livenessCheckStatus)
            self.animationVectorFinish(arcCircle: self.curArcCheck)
            self.viewModel.reqUpdateStatusPoint(isSuccess: livenessCheckStatus)
        } else {
            // out face zone or fail liveness check
//            print("Check fail!")
        }
    }
    
    private func calculateQuadrantI(centre: CGPoint, point: CGPoint) -> CGFloat {
        let r = distanceBetweenPoints(a: centre, b: point)
        let h = point.y - centre.y
        let angle = asin(h/r)
        return angle
    }
    
    private func calculateQuadrantII(centre: CGPoint, point: CGPoint) -> CGFloat {
        let r = distanceBetweenPoints(a: centre, b: point)
        let h = point.y - centre.y
        let angle = CGFloat.pi - asin(h/r)
        return angle
    }
    
    private func calculateQuadrantIII(centre: CGPoint, point: CGPoint) -> CGFloat {
        let r = distanceBetweenPoints(a: centre, b: point)
        let h = abs(point.y - centre.y)
        let angle = CGFloat.pi + asin(h/r)
        return angle
    }
    
    private func calculateQuadrantIV(centre: CGPoint, point: CGPoint) -> CGFloat {
        let r = distanceBetweenPoints(a: centre, b: point)
        let h = abs(point.y - centre.y)
        let angle = 2*CGFloat.pi - asin(h/r)
        return angle
    }
    
    private func checkAngularCoordinates(radian: CGFloat) -> ARC_CIRCLE {
        let degree = radian * (180/CGFloat.pi)
        
        if (22.5 < degree && degree < 67.5) {
            return ARC_CIRCLE.RIGHT_BOTTOM
        } else if (112.5 < degree && degree < 157.5) {
            return ARC_CIRCLE.LEFT_BOTTOM
        } else if (157.5 < degree && degree < 202.5) {
            return ARC_CIRCLE.LEFT_CENTER
        } else if (202.5 < degree && degree < 247.5) {
            return ARC_CIRCLE.LEFT_TOP
        } else if (292.5 < degree && degree < 337.5) {
            return ARC_CIRCLE.RIGHT_TOP
        } else if ((337.5 < degree && degree < 360) || (0 < degree && degree < 22.5)) {
            return ARC_CIRCLE.RIGHT_CENTER
        }
        return ARC_CIRCLE.NONE
    }
    
    private func checkAngularCoordinatesTest(radian: CGFloat, point: CGPoint) -> ARC_CIRCLE {
        let degree = radian * (180/CGFloat.pi)
        
        
//        //-- test GUI
//        print(degree)
//        descriptionLbl.text = "\(degree)"
        let positionConvert = lineCheckGUI.convert(self.centerFrontFaceMockDevice, from: transparentHolesView)
        let endPoint = CGPoint(x: point.x + cos(radian)*Constant.LIVENESS_CHECK_RADIUS_ZONE, y: point.y + sin(radian)*Constant.LIVENESS_CHECK_RADIUS_ZONE)

        let endPointConvert = lineCheckGUI.convert(endPoint, from: transparentHolesView)

        //design the path
        let path = UIBezierPath()
        path.move(to: positionConvert)
        path.addLine(to: endPointConvert)

        //design path in layer
        shapeLayerLine.path = path.cgPath
        shapeLayerLine.strokeColor = UIColor.blue.cgColor
        shapeLayerLine.lineWidth = 1.0

        lineCheckGUI.layer.addSublayer(shapeLayerLine)
//        //--
        
        if (22.5 < degree && degree < 67.5) {
            return ARC_CIRCLE.RIGHT_BOTTOM
        } else if (112.5 < degree && degree < 157.5) {
            return ARC_CIRCLE.LEFT_BOTTOM
        } else if (157.5 < degree && degree < 202.5) {
            return ARC_CIRCLE.LEFT_CENTER
        } else if (202.5 < degree && degree < 247.5) {
            return ARC_CIRCLE.LEFT_TOP
        } else if (292.5 < degree && degree < 337.5) {
            return ARC_CIRCLE.RIGHT_TOP
        } else if ((337.5 < degree && degree < 360) || (0 < degree && degree < 22.5)) {
            return ARC_CIRCLE.RIGHT_CENTER
        }
        return ARC_CIRCLE.NONE
    }
    
    private func checkZoneLivenessCheck(face: VisionFace) -> Bool {
//        let centerCircle = CGPoint(x: self.view.frame.width/2, y: self.view.frame.height/2)
        let rectFace = convertRectFaceVisionToDevice(face: face)
        let centerPointFace = CGPoint(x: rectFace.midX, y: rectFace.midY)
        
        if (distanceBetweenPoints(a: self.centerFrontFaceMockDevice, b: centerPointFace) > Constant.LIVENESS_CHECK_RADIUS_ZONE) {
            // action
            return true
        }
        return false
    }
    
    private func livenessCheck(face: VisionFace) -> Bool {
        if (livenessCheck_LeftCenter(face: face) ||
            livenessCheck_RightCenter(face: face) ||
            livcessCheck_RightBottom(face: face) ||
            livcessCheck_RightTop(face: face) ||
            livcessCheck_LeftTop(face: face) ||
            livcessCheck_LeftBottom(face: face)) {
            return true
        }
        return false
    }
    
    private func livenessCheck_LeftCenter(face: VisionFace) -> Bool {
        if (face.headEulerAngleY < -25 && face.headEulerAngleZ > -1) {
            return true
        }
        return false
    }

    private func livenessCheck_RightCenter(face: VisionFace) -> Bool {
        if (face.headEulerAngleY > 25 && face.headEulerAngleZ > -1) {
            return true
        }
        return false
    }

    private func livcessCheck_RightBottom(face: VisionFace) -> Bool {
        if (face.headEulerAngleY > 15 && face.headEulerAngleZ < 0) {
            return true
        }
        return false
    }
    
    private func livcessCheck_RightTop(face: VisionFace) -> Bool {
        if (face.headEulerAngleY > 20 && face.headEulerAngleZ > 0) {
            return true
        }
        return false
    }
    
    private func livcessCheck_LeftTop(face: VisionFace) -> Bool {
        if (face.headEulerAngleY > -15 && face.headEulerAngleZ < 0) {
            return true
        }
        return false
    }
    
    private func livcessCheck_LeftBottom(face: VisionFace) -> Bool {
        if (face.headEulerAngleY > -15 && face.headEulerAngleZ > 0) {
            return true
        }
        return false
    }
    
    private func convertRectFaceVisionToDevice(face: VisionFace) -> CGRect {
        let normalizedRect = CGRect(
            x: face.frame.origin.x / Constant.CAMERA_WIDTH,
            y: face.frame.origin.y / Constant.CAMERA_HEIGHT,
            width: face.frame.size.width / Constant.CAMERA_WIDTH,
            height: face.frame.size.height / Constant.CAMERA_HEIGHT
        )
        let standardizedRect =
            self.previewLayer.layerRectConverted(fromMetadataOutputRect: normalizedRect).standardized
        return standardizedRect
    }
    
    private func distanceBetweenPoints(a: CGPoint, b: CGPoint) -> CGFloat {
        let xDist = a.x - b.x
        let yDist = a.y - b.y
        return CGFloat(sqrt(xDist * xDist + yDist * yDist))
    }
    
    func displayToastMessage(_ message : String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - self.view.frame.size.width/3, y: self.view.frame.size.height-100, width: self.view.frame.size.width/1.5, height: 50))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

extension IdentifyFaceViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        // process set frame
        self.proccess(every: 1) {
            if timer != nil {
                timer?.invalidate()
                timer = nil
            }
            
            // check run process
            if self.isFaceProcessing { return }
            isFaceProcessing = true
            
            // check buffer
            guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
//                print("Failed to get image buffer from sample buffer.")
                return
            }
                    // copy buffer
//            let pixelCopy = imageBuffer.copy()
//            let ciImage: CIImage = CIImage(cvImageBuffer: pixelCopy).oriented(forExifOrientation: Int32(UIImage.Orientation.downMirrored.rawValue))
//            let imgCollect = UIImage(ciImage: ciImage)
//            imgArray.append(imgCollect)

            // face recognition
            let visionImage = VisionImage(buffer: sampleBuffer)
            let metadata = VisionImageMetadata()
            let visionOrientation = UIUtilities.visionImageOrientation(from: UIUtilities.imageOrientation(fromDevicePosition: .front))
            metadata.orientation = visionOrientation
            visionImage.metadata = metadata
            let imageWidth = CGFloat(CVPixelBufferGetWidth(imageBuffer))
            let imageHeight = CGFloat(CVPixelBufferGetHeight(imageBuffer))
            detectFacesOnDevice(in: visionImage, width: imageWidth, height: imageHeight)
            DispatchQueue.main.async {
                self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.actTimer), userInfo: nil, repeats: false)
            }
        }
    }
    
    @objc fileprivate func actTimer() {
    }
    
    func convertCIImageToUIImage(cmage:CIImage) -> UIImage {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
    private func imageFromCIImage(ciImage: CIImage) -> UIImage? {
        let context = CIContext()
        guard let cgImage = context.createCGImage(ciImage, from: ciImage.extent.standardized) else { return nil }
        
        return UIImage(cgImage: cgImage)
    }
}

extension IdentifyFaceViewController: WEEEkycDelegate {
    func NextPointDelegate(_ netStatus: CurrentStatusPoint) -> Void {
        print("Next Point \(netStatus)")
        switch netStatus {
        case .SUCCESS(let resp):
            self.isArcRequestCheck = self.convertNumToArc(num: resp.position)
            self.setArcRequestCheck(arcNumber: self.isArcRequestCheck)
            self.setupAnimVectorRotate(arcCircle: self.isArcRequestCheck)
            break
//        case .FAILED(let resp):
//
//            break
//        case .UNKNOWERROR(let resp):
//
//            break
            
        default: break
        }
    }
    func currentStatusDelegate(_ curStatus: CurrentStatusPoint) -> Void {
        print("Current Point \(curStatus)")
        switch curStatus {
        case .SUCCESS(let resp):
            self.nextRequestArcProccess = true
            self.listLivenessCheckStatus.append(resp.status)
            break
//        case .FAILED(let resp):
//
//            break
//        case .UNKNOWERROR(let resp):
//
//            break
            
        default: break
        }
    }
    
    func timeoutStatusDelegate(_ status: CurrentStatusPoint) -> Void {
        switch status {
        case .SUCCESS(let resp):
            self.nextRequestArcProccess = true
            self.listLivenessCheckStatus.append(resp.status)
            self.curArcCheck = self.isArcRequestCheck
            self.animArcSelected(arcNumber: curArcCheck, successful: false)
            self.animationVectorFinish(arcCircle: self.curArcCheck)
        default: break
        }
    }
    
    func finishStatusDelegate() -> Void {
        print("Finish Point")
        print(self.listLivenessCheckStatus)
        
    }
}
