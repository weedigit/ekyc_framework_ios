//
//  WEECameraView.swift
//  EkycFramework
//
//  Created by Nguyen Thanh Duc on 2/9/20.
//  Copyright © 2020 Wee. All rights reserved.
//

import UIKit
import Ekyc

@IBDesignable
public class WEECameraView: UIView {

    @IBOutlet weak var displayCameraView: UIView!
    @IBOutlet weak var croppedView: WEEShapeCroppedView!
    
    fileprivate let cameraController = CameraController()
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        print("Awake From Nib =====")
        self.initUI()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        print("Init Frame")
        self.initUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        print("Init Coder")
        self.initUI()
    }
    
    fileprivate func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: WEECameraView.self), bundle: bundle)
        guard let summaryView = nib.instantiate(withOwner: self, options: nil).first as? UIView else {
            return UIView()
        }
        return summaryView
    }
    
    fileprivate func initUI() {
//        let view = self.loadNib()
//        view.frame = self.frame
//        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        self.addSubview(view)
//        Bundle.main.loadNibNamed("WECameraView", owner: self, options: nil)
        
        cameraController.prepare { [unowned self] (error) in
            if let error = error {
                print("configureCameraController", error.localizedDescription)
            }
            try? self.cameraController.displayPreview(on: self.displayCameraView)
            self.cameraController.delegate = self
            DispatchQueue.main.async {
             self.cameraController.startCamera()
            }
        }
        
//        EkycInitLibrary(self)
    }
    
}

//extension WEECameraView: EkycEkycDelegateProtocol {
//    public func next(_ num: Int) {
//        print(num)
//    }
//}

extension WEECameraView: CameraControllerDelegate {
    func getFaceBounds(bounds: CGRect?) {
        
    }
    
    func getImage(img: CIImage) {
        
    }
    
    
}
