//
//  WEEShapeCroppedView.swift
//  EkycFramework
//
//  Created by Nguyen Thanh Duc on 2/7/20.
//  Copyright © 2020 Wee. All rights reserved.
//

import UIKit

class WEEShapeCroppedView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.croppedView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.croppedView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.croppedView()
    }
    
    fileprivate func croppedView() {
        var roundedRect = self.bounds
        roundedRect.origin.x = roundedRect.size.width / 4
        roundedRect.origin.y = roundedRect.size.height / 4
        roundedRect.size.width = roundedRect.size.width / 2
        roundedRect.size.height = roundedRect.size.height / 2

        let path = UIBezierPath(rect: self.bounds)
        let croppedPath = UIBezierPath(rect: roundedRect)
        path.append(croppedPath)
        path.usesEvenOddFillRule = true

        let mask = CAShapeLayer()
        mask.path = path.cgPath
        mask.fillRule = .evenOdd
        self.layer.mask = mask
    }
    
}
