//
//  WEEExtractIDCardResultViewController.swift
//  EkycFramework
//
//  Created by Nguyen Thanh Duc on 2/18/20.
//  Copyright © 2020 Wee. All rights reserved.
//

import UIKit

public class WEEExtractIDCardResultViewController: UIViewController {

    @IBOutlet weak var lblIDCard: UILabel!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblBirthday: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblNational: UILabel!
    @IBOutlet weak var lblHometown: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblExpireDate: UILabel!
    @IBOutlet weak var imgCardType: UIImageView!
    
    var extractFrontData: ReqEkyc.Response.RespExtractFrontData?
    var extractBackData: ReqEkyc.Response.RespExtractBackData?
    
    public override var prefersStatusBarHidden: Bool {
        return false
    }
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let front = self.extractFrontData {
            self.lblIDCard.text = front.Number
            self.lblFullName.text = front.FullName
            self.lblBirthday.text = front.BirthDay
            self.lblHometown.text = front.HomeTown
            self.lblAddress.text = front.Address
        }
        
        if let back = self.extractBackData {
            self.lblExpireDate.text = back.Valid
        }
    }
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDoneClick(_ sender: UIButton) {
        let podBundle = Bundle(for: IdentifyFaceViewController.self)
        let bundleURL = podBundle.url(forResource: "EkycFramework", withExtension: "bundle")
        let bundle = Bundle(url: bundleURL!)!
        
        let viewController = UIStoryboard(name: "IdentifyFace", bundle: bundle).instantiateViewController(withIdentifier: "IdentifyFace") as? IdentifyFaceViewController
        if let vc = viewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func btnTryAgainClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
