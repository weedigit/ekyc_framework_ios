//
//  Constant.swift
//  weeid
//
//  Created by LV on 2/14/20.
//  Copyright © 2020 LV. All rights reserved.
//

import UIKit

class Constant: NSObject {
    static let CAMERA_WIDTH: CGFloat = 1280
    static let CAMERA_HEIGHT: CGFloat = 720
    static let WIDTH_CAMERA_VIEW: CGFloat = 720
    static let HEIGHT_CAMERA_VIEW: CGFloat = 1280
    static let SIZE_ZONE = CGSize(width: WIDTH_CAMERA_VIEW/6,height: HEIGHT_CAMERA_VIEW/6)
    // UI
    static let LIVENESS_SPACE_CIRCLE: CGFloat = 27
    static let LIVENESS_MIN_SPACE_ANIM_CIRCLE: CGFloat = 7
    static let ANIMATION_TIME = 0.75
    //--
    static let LIVENESS_CHECK_RADIUS_DETECT_FRONT_FACE: CGFloat = 50
    static let LIVENESS_CHECK_RADIUS_ZONE: CGFloat = 30
    static let LIVENESS_CHECK_ROTATE_FRONT_FACE_MIN: CGFloat = -5
    static let LIVENESS_CHECK_ROTATE_FRONT_FACE_MAX: CGFloat = 5
//    static let LIVENESS_CHECK_ROTATE_LEFT_MIN: CGFloat = -25
//    static let LIVENESS_CHECK_ROTATE_RIGHT_MIN: CGFloat = 25
//    static let LIVENESS_CHECK_ROTATE_Z_MIN = -10
//    static let LIVENESS_CHECK_ROTATE_Z_MAX = 10
}

// Color definite
class Colour: NSObject {
    static let arcDefaultColor = UIColor(red: 0.2, green: 0.678, blue: 1, alpha: 0.2).cgColor
    static let arcSelectedColor = UIColor.red.cgColor
}

enum ARC_CIRCLE {
    case NONE
    case LEFT_TOP
    case LEFT_CENTER
    case LEFT_BOTTOM
    case RIGHT_TOP
    case RIGHT_CENTER
    case RIGHT_BOTTOM
}
