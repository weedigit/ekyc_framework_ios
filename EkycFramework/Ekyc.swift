//
//  Ekyc.swift
//  EkycFramework
//
//  Created by Nguyen Thanh Duc on 2/19/20.
//  Copyright © 2020 Wee. All rights reserved.
//

import Foundation
import Ekyc

public class Ekyc {
    public static func InitLibraries() {
        let podBundle = Bundle(for: Ekyc.self)

        let bundleURL = podBundle.url(forResource: "EkycFramework", withExtension: "bundle")
        let bundle = Bundle(url: bundleURL!)!
        
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Black.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Black Italic.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Bold.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Bold Italic.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Heavy.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Heavy Italic.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Italic.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Light.otf", bundle: bundle)
        
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Light Italic.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Medium.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Medium Italic.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Regular.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy SemiBold.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy SemiBold Italic.otf", bundle: bundle)
        
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Thin.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Thin Italic.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy XBold.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy XBold Italic.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Xlight.otf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Gilroy Xlight Italic.otf", bundle: bundle)
        
        UIFont.jbs_registerFont(withFilenameString: "SVN-Rajdhani Bold.ttf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Rajdhani Light.ttf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Rajdhani Medium.ttf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Rajdhani Regular.ttf", bundle: bundle)
        UIFont.jbs_registerFont(withFilenameString: "SVN-Rajdhani SemiBold.ttf", bundle: bundle)
    }
}
