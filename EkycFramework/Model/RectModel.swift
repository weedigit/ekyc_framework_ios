//
//  RectModel.swift
//  EkycFramework
//
//  Created by Nguyen Thanh Duc on 2/12/20.
//  Copyright © 2020 Wee. All rights reserved.
//

import Foundation

struct Rect: Codable {
    var X: Float64
    var Y: Float64
    var Width: Float64
    var Height: Float64
}

struct ReqEkyc {
    struct Request {
        struct ReqExtractData {
            let frontImage: Data
        }
    }
    
    struct Response {
        struct RespExtractFrontData: Codable {
            let SessionID: String
            let Number: String
            let BirthDay: String
            let Code: Int
            let Message: String
            let FullName: String
            let HomeTown: String
            let Address: String
        }
        
        struct RespExtractBackData: Codable {
            let Code: Int
            let Message: String
            let Valid: String
        }
        
        struct RespCurrentStatusPoint: Codable {
            let position: Int
            let status: Bool
        }
    }
    
}

enum ExtractFrontIDCardStatus {
    case SUCCESS(ReqEkyc.Response.RespExtractFrontData)
    case FAILED(ReqEkyc.Response.RespExtractFrontData)
    case UNKNOWERROR(ReqEkyc.Response.RespExtractFrontData)
}

enum ExtractBackIDCardStatus {
    case SUCCESS(ReqEkyc.Response.RespExtractBackData)
    case FAILED(ReqEkyc.Response.RespExtractBackData)
    case UNKNOWERROR(ReqEkyc.Response.RespExtractBackData)
}

enum CurrentStatusPoint {
    case SUCCESS(ReqEkyc.Response.RespCurrentStatusPoint)
    case FAILED(ReqEkyc.Response.RespCurrentStatusPoint)
    case UNKNOWERROR(ReqEkyc.Response.RespCurrentStatusPoint)
}
